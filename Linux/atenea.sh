#!/bin/bash
########################################################################
#  Gittex
#  Copyright (C) 2018  Zittec
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################################################
cd "$(dirname "$0")"
ESTE_DIRECTORIO="$(dirname "$0")"
echo E
{
  python -V
} || {
  apt-get install python
}
notify-send --app-name=Atenea --icon=$ESTE_DIRECTORIO/icons/atenea.ico "Iniciando"
python get-pip.py
python src.pyw
