#!/usr/bin/env python
# -*- coding: utf-8 -*-
def install_and_import(package):
	import importlib
	try:
		importlib.import_module(package)
	except ImportError:
		import pip
		pip.main(['install', package])
		print("Se instaló el paquete "+package+" previamente NO instalado")
	finally:
		globals()[package] = importlib.import_module(package)
		print("Se importo: "+package)
try:
	import requests
except ImportError:
	install_and_import('requests')

#coding: utf-8
import sys
import getopt
import os
import inspect
#print (os.path.realpath(__file__))
path=os.path.realpath(__file__)
#print (os.path.getctime(path))
#print (sys.version)
import hashlib
import webbrowser
import subprocess
#print (webbrowser._browsers)
from Tkinter import *
#from uuid import getnode as get_mac
#mac = get_mac()
#mac= str(mac)
mac= str(os.path.getctime(path))
#print (mac)
macmd5= hashlib.md5(mac).hexdigest()
from time import localtime, strftime
import time
#print (strftime("%Y-%m-%d %H:%M:%S", gmtime()))

#print(ahora)
preclave0=str(hashlib.md5(mac).hexdigest())
#print(preclave0)
def obtenerOpciones(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi,o:",["",""])
   except getopt.GetoptError:
      print("Error:")
      print 'Veti.py [-opcion] [-argumento]'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'Veti.py [-opcion] [-argumento]'
         sys.exit()
      elif ("-i") in opt:
         import time
         time.sleep(5)
obtenerOpciones(sys.argv[1:])

#print (clave_txt)
web="http://www.alegora.mx/i/";
url = web+'login.php'
parametros = {'clave': preclave0}
r = requests.post(url, data=parametros)

if r.status_code==200:
	if r.text=='registrar':
		b = webbrowser.get('firefox')
		b.open(web+'standby.php?clave='+preclave0)
	if r.text=='error2':
		master0 = Tk()
		Label(master0, text="¡PROHIBIDO! Este equipo ha sido bloqueado, cese y desista").grid(row=0)
		Button(master0, text='Salir', command=master0.quit).grid(row=3, column=0, sticky=W, pady=4)
		master0.mainloop()
	if r.text=='error1':
		b = webbrowser.get('firefox')
		b.open(web+'standby.php?clave='+preclave0)
	if r.text=='error0':
		def show_entry_fields():
		   #print("Nombre del equipo: %s" % (e1.get()))
		   #b = webbrowser.get('firefox')
		   #b.open_new(web+'login.php?clave='+clave_txt)
		   	global master
			url = web+'registrarequipo.php'
			parametros = {'clave': preclave0, 'nombre': e1.get()}
			r = requests.post(url, data=parametros)
			b = webbrowser.get('firefox')
		   	b.open(web+'standby.php?clave='+preclave0)
			master.quit()
		master = Tk()
		Label(master, text="Nombre del equipo").grid(row=0)

		e1 = Entry(master)
		e1.grid(row=0, column=1)

		Button(master, text='Salir', command=master.quit).grid(row=3, column=0, sticky=W, pady=4)
		Button(master, text='Enviar', command=show_entry_fields).grid(row=3, column=1, sticky=W, pady=4)
		master.mainloop()
	if r.text=='exito':

		ahora=strftime("%Y-%m-%d-%H:%M:%S", localtime())
		preclave2=preclave0+ahora
		#print (preclave2)
		clave=hashlib.md5(preclave2).hexdigest()
		clave_txt=str(clave)
		b = webbrowser.get('firefox')
		b.open(web+'login.php?clave='+clave_txt)
		exit()
